package mx.gob.gau.client;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public interface KeycloakServicesInterface {

//    get access token
    @POST
    @Path("/realms/{realmName}/protocol/openid-connect/token")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    Response getConfidentialAccessToken(
        @PathParam("realmName") String realmName,
        @FormParam("grant_type") String grantType,
        @FormParam("client_id") String clientId,
        @FormParam("client_secret") String clientSecret
        );



//    Send email
    @PUT
    @Path("/admin/realms/{realmName}/users/{userId}/execute-actions-email")
    @Consumes(MediaType.APPLICATION_JSON)
    Response askPasswordUpdate(
        @HeaderParam(HttpHeaders.AUTHORIZATION) String token,
        @PathParam("realmName") String realmName,
        @PathParam("userId") String userId);

}

