package mx.gob.gau.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ConnectionCredentials {

    @JsonProperty("grant_type")
    private String grantType;

        @JsonProperty("client_id")
    private String clientId;

    @JsonProperty("client_secret")
    private String clientSecret;

    public String getGrantType() { return grantType; }

    public void setGrantType(String grantType) { this.grantType = grantType; }

    public String getClientId() { return clientId; }

    public void setClientId(String clientId) { this.clientId = clientId; }

    public String getClientSecret() { return clientSecret; }

    public void setClientSecret(String clientSecret) { this.clientSecret = clientSecret; }
}
