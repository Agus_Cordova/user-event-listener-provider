package mx.gob.gau.client;

import mx.gob.gau.client.model.ConnectionCredentials;
import mx.gob.gau.client.model.TokenResponse;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;


public class KeycloakRestProxyClient {

    public static final Logger LOGGER = LoggerFactory.getLogger(KeycloakRestProxyClient.class);
//    public static final String PATH = "http://127.0.0.1:8080/";
    String path;

    ResteasyClient client;
    ResteasyWebTarget target;
    KeycloakServicesInterface proxy;

    public KeycloakRestProxyClient(String path) {
         this.path = path;
         this.client = new ResteasyClientBuilder().build();
         this.target = client.target(UriBuilder.fromPath(this.path));
         this.proxy = target.proxy(KeycloakServicesInterface.class);
    }


    public String getAccessToken(String realm, ConnectionCredentials creds) {
        String token;
        Response restResponse = proxy.getConfidentialAccessToken(
                realm,
                creds.getGrantType(),
                creds.getClientId(),
                creds.getClientSecret());

        // get entity json and extract access token
        TokenResponse tokenResponse = (TokenResponse)restResponse.getEntity();
        token = tokenResponse.getAccessToken();
        if (token == null) {
            //  throw new Exception
        }
        LOGGER.info("++++++getConfidentialAccessToken:" + token);

        return token;
    }

    public boolean authAndSendEmail(String userId, String realm, ConnectionCredentials creds) {
        String token = this.getAccessToken(realm, creds);
        return this.sendEmail(token, realm, userId);
    }

    private boolean sendEmail(String token, String realm, String userId) {
        Response resp = this.proxy.askPasswordUpdate(token, realm, userId);
        if (resp.getStatus() == 204) {
            return true;
        }

        return false;
    }

}
