package mx.gob.gau.provider;

import mx.gob.gau.client.model.ConnectionCredentials;
import mx.gob.gau.client.KeycloakRestProxyClient;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class GAUUserEventListenerProvider implements EventListenerProvider {

    public static final Logger LOGGER = LoggerFactory.getLogger(GAUUserEventListenerProvider.class);
    public static final String LOGIN_ERROR = "LOGIN_ERROR";
    public static final String USER_DISABLED = "user_disabled";
    public static final String UPDATE_PASSWORD = "UPDATE_PASSWORD";
    public static final String RESET_PASSWORD = "RESET_PASSWORD";


    public GAUUserEventListenerProvider() {
    }

    @Override
    public void onEvent(Event event) {
        LOGGER.info("***********  @Event:  : " + toString(event) + " ***********");
        String eventType = event.getType().toString();
        String eventError = event.getError();
        LOGGER.info("***********  EventType:  : "+ eventType + ", EventError: " + eventError+ " ***********");

        if (eventType.equals(LOGIN_ERROR) && eventError.equals(USER_DISABLED)) {
            String username = "";
            String userId = event.getUserId();
            String realmId = event.getRealmId();
            if (event.getDetails() != null) {
                Map<String, String> details = event.getDetails();
                username = details.get("username");

            }
            LOGGER.info("   ++++++++ User = " + username +", id: "+ userId + ", has been blocked!");
            LOGGER.info("+++++++ Attempting send email...");
//          Send password reset email to user
            try {
                sendUpdatePassEmail(userId, realmId);
            } catch (IOException e) {
                LOGGER.error("+++++++++++ Unable to send update password email to " + userId);
                LOGGER.error(e.getMessage());
            }
        }

        if (event.getType().equals(UPDATE_PASSWORD) ) {
            //  TODO catch update (or reset?) password event for user and re-enable it
            LOGGER.info("+++++++ UPDATED PASSWORD...");
        }
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
        LOGGER.info("========= @AdminEvent: "+ toString(adminEvent) +" , boolean: " + b + "=========");

    }

    @Override
    public void close() {

        LOGGER.info("GAUUserEventListenerProvider <------- closed");
    }

    private void sendUpdatePassEmail(String userId, String realmId) throws IOException {

        // get connection values from pom properties
        PropertiesReader props = new PropertiesReader("listener-provider.properties");

        KeycloakRestProxyClient restClient = new KeycloakRestProxyClient(props.getProperty("connection.base-url"));

        ConnectionCredentials creds = new ConnectionCredentials();
        creds.setGrantType(props.getProperty("connection.grant-type"));
        creds.setClientId(props.getProperty("connection.client-id"));
        creds.setClientSecret(props.getProperty("connection.client-secret"));


        boolean sentEmail = restClient.authAndSendEmail(userId, realmId, creds);

    }


    private String toString(Event event) {

        StringBuilder sb = new StringBuilder();

        sb.append("type=");
        sb.append(event.getType());

        sb.append(", realmId=");
        sb.append(event.getRealmId());

        sb.append(", clientId=");
        sb.append(event.getClientId());

        sb.append(", userId=");
        sb.append(event.getUserId());

        sb.append(", ipAddress=");
        sb.append(event.getIpAddress());

        if (event.getError() != null) {
            sb.append(", error=");
            sb.append(event.getError());
        }

        if (event.getDetails() != null) {
            for (Map.Entry<String, String> e : event.getDetails().entrySet()) {
                sb.append(", ");
                sb.append(e.getKey());
                if (e.getValue() == null || e.getValue().indexOf(' ') == -1) {
                    sb.append("=");
                    sb.append(e.getValue());
                } else {
                    sb.append("='");
                    sb.append(e.getValue());
                    sb.append("'");
                }
            }
        }

        return sb.toString();

    }


    private String toString(AdminEvent adminEvent) {

        StringBuilder sb = new StringBuilder();

        sb.append("operationType=");
        sb.append(adminEvent.getOperationType());

        sb.append(", realmId=");
        sb.append(adminEvent.getAuthDetails().getRealmId());

        sb.append(", clientId=");
        sb.append(adminEvent.getAuthDetails().getClientId());

        sb.append(", userId=");
        sb.append(adminEvent.getAuthDetails().getUserId());

        sb.append(", ipAddress=");
        sb.append(adminEvent.getAuthDetails().getIpAddress());

        sb.append(", resourcePath=");
        sb.append(adminEvent.getResourcePath());

        if (adminEvent.getError() != null) {
            sb.append(", error=");
            sb.append(adminEvent.getError());
        }

        return sb.toString();
    }
}


class PropertiesReader {
    private Properties properties;

    public PropertiesReader(String propertyFileName) throws IOException {
        InputStream is = getClass().getClassLoader()
                .getResourceAsStream(propertyFileName);
        this.properties = new Properties();
        this.properties.load(is);
    }

    public String getProperty(String propertyName) {
        return this.properties.getProperty(propertyName);
    }
}